#!/usr/bin/python3
"""
TODO

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always     The bot won't ask for confirmation when putting a page
-summary    Set the action summary message for the edit.
-major      If used, the edit will be saved without the "minor edit" flag

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)
import mwparserfromhell
from mwparserfromhell.wikicode import Wikicode, Template

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': 'Change from deprecated parameters',
	'minor': True,
}

def replace_template_arg(template: Template, param_from, param_to):
	if template.has(param_from):
		if template.has(param_to) and len(template.get(param_to).value.strip()) > 0:
			print(f'{param_from} -> {param_to}')
			print(f'  -- from: {template.get(param_from).value.strip()}')
			print(f'  -- len : {len(template.get(param_from).value.strip())}')
			print(f'  -- to  : {template.get(param_to).value.strip()}')
			print(f'  -- len : {len(template.get(param_to).value.strip())}')
			if template.get(param_to).value.strip().lower() == template.get(param_from).value.strip().lower() or \
				len(template.get(param_to).value.strip()) >= len(template.get(param_from).value.strip()):
				template.remove(param_from)
			# Will handle non-equal parameters later
		else:
			template.get(param_from).name = param_to

class SongInfoUpdater(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
):

	"""
	TODO
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: 'pywikibot.page.BasePage') -> bool:
		title = page.title()
		text = page.text

		page.protection()
		if not page.has_permission():
			pywikibot.warning(f"{title} is protected: this account can't edit it! Skipping...")
			return True

		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""
		page = self.current_page

		title = page.title()
		text = page.text
		# print(title)
		# print(text)

		code: Wikicode = mwparserfromhell.parse(text)

		for template in code.filter_templates(
			matches=lambda x: str(x.name) in ("ModInfo"),
		):

			replace_template_arg(template, "coder", "programmer")
			replace_template_arg(template, "music", "composer")
				
		text = str(code)
		
		# print(text)
		# with open('test.mediawiki', 'w', encoding="utf-8") as f:
		# 	f.write(text)

		self.put_current(text, summary=self.opt.summary, minor=self.opt.minor)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = SongInfoUpdater(generator=gen, **args)
		bot.run()

if __name__ == '__main__':
	main()
