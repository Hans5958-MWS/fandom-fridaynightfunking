#!/usr/bin/python3
"""
Updates {{TLDR}} and {{Notices}} values based on existing notices.

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always     The bot won't ask for confirmation when putting a page
-summary    Set the action summary message for the edit.
-major      If used, the edit will be saved without the "minor edit" flag

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
from mwparserfromhell.wikicode import Wikicode, Template
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)
import mwparserfromhell

# Name on parameter (Parameter) -> Name on TLDR (TLDR Name)
NOTICES_DICT = {
	# "CreatorHelp": "Creator Help",
	"Bad": "Low Quality Article",
	"Broken": "Broken Contents",
	"Disambiguation": "Disambiguation",
	"Discontinued": "Discontinued Mod",
	"Fan-Favorite": "Fan Favorite",
	"Fan-Made": "Fan-Made",
	"File Quality": "Low File Quality",
	"GoldenThrone": "Golden Throne",
	"Good": "High Quality Article",
	"Hold": "On Hold",
	"Image Quality": "Low File Quality",
	"Language": "Foreign Language",
	"Long": "Large Page",
	"Lost": "Lost Mod",
	"Outdated": "Outdated Page",
	"Partially Lost": "Partially Lost",
	"Place Holder": "Placeholder Used",
	"Proofread": "Proofread",
	"Protected": "Protected",
	"ResolvedControversy": "Resolved Controversy",
	"Storage": "Storage",
	"Stub": "Unfinished Page",
	"Unavailable": "Unavailable Download",
	"Unfinished": "Unreleased Mod",
	"UnfinishedSong": "Unfinished Song",
	"Unreleased": "Upcoming Content",
	"UpcomingContent": "Upcoming Content",
}

# Template name -> Name on parameter (Parameter)
NOTICES_PARAMETER_DICT = {
	# "Creator Help": "CreatorHelp",
	"bad": "Bad",
	"broken": "Broken",
	"discontinued": "Discontinued",
	"disambiguation": "Disambiguation",
	"fan-favorite": "Fan-Favorite",
	"show-fan-favorite-notice": "Fan-Favorite",
	"fan-made": "Fan-Made",
	"file-quality": "File Quality",
	"golden-throne": "GoldenThrone",
	"good": "Good",
	"show-good-notice": "Good",
	"hold": "Hold",
	"image-quality": "Image Quality",
	"language": "Language",
	"long": "Long",
	"lost": "Lost",
	"outdated": "Outdated",
	"partially-lost": "Partially Lost",
	"placeholder": "Place Holder",
	"proofread": "Proofread",
	"protected": "Protected",
	"resolved-controversy": "ResolvedControversy",
	"storage": "Storage",
	"stub": "Stub",
	"unavbailable": "Unavailable",
	"unfinished": "Unfinished",
	"unfinished-song": "UnfinishedSong",
	"unreleased": "Unreleased",
	"upcoming-content": "UpcomingContent",
}

# Name on TLDR (TLDR Name)
WARNINGS_DICT = {
	# "SongInfoLoud": "Loud Audios", # unconventional
	"Controversial": "Controversial",
	"Copyrighted": "Copyrighted",
	"CW": "Content Warning",
	"LoudMulti": "Loud Audios", # unused?
	"Spoilers": "Spoilers",
	"TW": "Trigger Warning",
}

# Name on parameter (Parameter)
WARNINGS_PARAMETER_DICT = {
	"controversial": "Controversial",
	"copyrighted": "Copyrighted",
	"content-warning": "CW",
	"spoilers": "Spoilers",
	"trigger-warning": "TW",
}

REDIRECT_TEMPLATES = {
	"Locked": "Protected"
}

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': 'Update notice TLDR values',
	'minor': True,
}

class TLDRUpdater(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
):

	"""
	Updates {{TLDR}} and {{Notices}} values based on existing notices.
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: 'pywikibot.page.BasePage') -> bool:
		title = page.title()
		text = page.text

		# page.protection()
		# if not page.has_permission():
		# 	pywikibot.warning(f"{title} is protected: this account can't edit it! Skipping...")
		# 	return True

		code: Wikicode = mwparserfromhell.parse(text)

		tldr_template = code.filter_templates(
			matches=lambda x: x.name.matches('TLDR'),
		)

		notices_template = code.filter_templates(
			matches=lambda x: x.name.matches('Notices'),
		)

		if len(tldr_template) + len(notices_template) != 1:
			pywikibot.warning(f"{title} has more or less than 1 of the templates! Skipping...")
			return True

		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""
		page = self.current_page

		title = page.title()
		text = page.text
		# print(title)
		# print(text)

		code: Wikicode = mwparserfromhell.parse(text)
		tldr_template: Template = None
		notices_template: Template = None
		warnings_parameter = None
		notices_parameter = None
		outside_templates: bool = False

		new_warnings_list = []
		new_notices_list = []

		for template in code.filter_templates():
			if template.name.matches("TLDR"):
				tldr_template = template
				if tldr_template.has('1'):
					warnings_parameter = tldr_template.get(1)
				if tldr_template.has('2'):
					notices_parameter = tldr_template.get(2)
		
			if template.name.matches("Notices"):
				notices_template = template
				if notices_template.has('warnings'):
					warnings_parameter = notices_template.get('warnings')
				if notices_template.has('notices'):
					notices_parameter = notices_template.get('notices')

				for param in notices_template.params:
					if str(param.name) in WARNINGS_PARAMETER_DICT and WARNINGS_DICT[WARNINGS_PARAMETER_DICT[str(param.name)]] not in new_warnings_list:
						new_warnings_list.append(WARNINGS_DICT[WARNINGS_PARAMETER_DICT[str(param.name)]])
					if str(param.name) in NOTICES_PARAMETER_DICT and NOTICES_DICT[NOTICES_PARAMETER_DICT[str(param.name)]] not in new_notices_list:
						new_notices_list.append(NOTICES_DICT[NOTICES_PARAMETER_DICT[str(param.name)]])

			elif str(template.name) in WARNINGS_DICT and WARNINGS_DICT[str(template.name)] not in new_warnings_list:
				new_warnings_list.append(WARNINGS_DICT[str(template.name)])
				outside_templates = True
		
			elif str(template.name) in NOTICES_DICT and NOTICES_DICT[str(template.name)] not in new_notices_list:
				new_notices_list.append(NOTICES_DICT[str(template.name)])
				outside_templates = True
			# else:
			# 	print(template.name)

		old_warnings: str = ''
		old_warnings_list: list = []
		old_notices: str = ''
		old_notices_list: list = []

		if warnings_parameter:
			old_warnings = str(warnings_parameter.value).strip()
			old_warnings_list = old_warnings.rpartition('.')[0].split(', ')
			old_warnings_list.sort()
		if notices_parameter:
			old_notices = str(notices_parameter.value).strip()
			old_notices_list = old_notices.rpartition('.')[0].split(', ')
			old_notices_list.sort()

		if 'Content Warning' in new_warnings_list and 'Trigger Warning' in new_warnings_list:
			cw_index = new_warnings_list.index('Content Warning')
			tw_index = new_warnings_list.index('Trigger Warning')
			new_warnings_list[min(cw_index, tw_index)] = "Content/Trigger Warning"
			new_warnings_list.pop(max(cw_index, tw_index))

		new_warnings = ', '.join(new_warnings_list) + "."
		if new_warnings == '.':
			new_warnings = ''
		new_notices = ', '.join(new_notices_list) + "."
		if new_notices == '.':
			new_notices = ''

		print('Old Warnings: ' + old_warnings)
		print('New Warnings: ' + new_warnings)
		print('Old Notices: ' + old_notices)
		print('New Notices: ' + new_notices)

		if notices_template:
			warnings_parameter = notices_template.add('warnings', new_warnings)
			notices_parameter = notices_template.add('notices', new_notices)
			if not new_warnings or not outside_templates:
				notices_template.remove('warnings')
			if not new_notices or not outside_templates:
				notices_template.remove('notices')
		elif tldr_template:
			warnings_parameter = tldr_template.add(1, new_warnings)
			notices_parameter = tldr_template.add(2, new_notices)
			if not new_warnings:
				tldr_template.remove(1)
			if not new_notices:
				tldr_template.remove(2)

		# print(old_notices_list, new_notices_list)
		if old_warnings_list == sorted(new_warnings_list) and old_notices_list == sorted(new_notices_list) and notices_template and outside_templates:
			pywikibot.info('Warnings and notices are identical. Skipping!')
			return True

		text = str(code)
		# print(text[:1000])

		self.put_current(text, summary=self.opt.summary, minor=self.opt.minor)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = TLDRUpdater(generator=gen, **args)
		bot.run()

if __name__ == '__main__':
	main()
