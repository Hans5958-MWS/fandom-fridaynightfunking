#!/usr/bin/python3
"""
Migrates all song info templates with the newer, singular {{SongInfo}}.

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always     The bot won't ask for confirmation when putting a page
-summary    Set the action summary message for the edit.
-major      If used, the edit will be saved without the "minor edit" flag

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)
import mwparserfromhell
from mwparserfromhell.wikicode import Wikicode, Template

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': 'Migrate to newer {{SongInfo}}',
	'minor': True,
}

def replace_template_arg(template: Template, param_from, param_to):
	if template.has(param_from):
		template.get(param_from).name = param_to

class SongInfoUpdater(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
):

	"""
	Migrates all song info templates with the newer, singular {{SongInfo}}.
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: 'pywikibot.page.BasePage') -> bool:
		# title = page.title()

		# page.protection()
		# if not page.has_permission():
		# 	pywikibot.warning(f"{title} is protected: this account can't edit it! Skipping...")
		# 	return True

		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""
		page = self.current_page

		title = page.title()
		text = page.text
		# print(title)
		# print(text)

		code: Wikicode = mwparserfromhell.parse(text)

		for template in code.filter_templates(
			matches=lambda x: str(x.name) in ("SongInfo", "SongInfoGradient", "SongInfoLoud"),
		):

			replace_template_arg(template, "px", "iconsize")
			replace_template_arg(template, "px1", "iconsize1")
			replace_template_arg(template, "px2", "iconsize2")
			replace_template_arg(template, "px3", "iconsize3")
			replace_template_arg(template, "px4", "iconsize4")
			replace_template_arg(template, "px5", "iconsize5")
			replace_template_arg(template, "px6", "iconsize6")

			replace_template_arg(template, "file", "audio")
			replace_template_arg(template, "file1", "audio1")
			replace_template_arg(template, "file2", "audio2")
			replace_template_arg(template, "file3", "audio3")
			replace_template_arg(template, "file4", "audio4")
			replace_template_arg(template, "file5", "audio5")
			replace_template_arg(template, "file6", "audio6")

			replace_template_arg(template, "mp3px", "audiosize")
			replace_template_arg(template, "mp3px1", "audiosize1")
			replace_template_arg(template, "mp3px2", "audiosize2")
			replace_template_arg(template, "mp3px3", "audiosize3")
			replace_template_arg(template, "mp3px4", "audiosize4")
			replace_template_arg(template, "mp3px5", "audiosize5")
			replace_template_arg(template, "mp3px6", "audiosize6")

			replace_template_arg(template, "timeStamp", "audiodesc")
			replace_template_arg(template, "timeStamp1", "audiodesc1")
			replace_template_arg(template, "timeStamp2", "audiodesc2")
			replace_template_arg(template, "timeStamp3", "audiodesc3")
			replace_template_arg(template, "timeStamp4", "audiodesc4")
			replace_template_arg(template, "timeStamp5", "audiodesc5")
			replace_template_arg(template, "timeStamp6", "audiodesc6")

			replace_template_arg(template, "loud", "loud")
			replace_template_arg(template, "WarningIntensity", "loudintensity")
			replace_template_arg(template, "Bass", "bass")

			replace_template_arg(template, "BassIntensity", "bassintensity")
			replace_template_arg(template, "Warning", "warning")
			replace_template_arg(template, "WarningExtra", "warningextra")

			if template.name.matches('SongInfoLoud'):
				if template.has("bass"):
					if template.get("bass").value != "Only" and not template.has("warning"):
						template.add('loud', 1)
				elif not template.has("warning"):
					template.add('loud', 1)
			template.name = "SongInfo"

				
		text = str(code)
		
		# print(text)
		# with open('test.mediawiki', 'w', encoding="utf-8") as f:
		# 	f.write(text)

		self.put_current(text, summary=self.opt.summary, minor=self.opt.minor)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = SongInfoUpdater(generator=gen, **args)
		bot.run()

if __name__ == '__main__':
	main()
