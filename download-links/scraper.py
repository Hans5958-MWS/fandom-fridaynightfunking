import requests
from bs4 import BeautifulSoup

url = "https://fridaynightfunking.fandom.com/wiki/Special:WhatLinksHere/File:Gamebanana.png?limit=500&namespace=0"
f = open('pages.txt', 'a+', encoding='utf-8')
pages = 0

while url:
	pages += 1
	soup = BeautifulSoup(requests.get(url).content, "html.parser")
	results = soup.find(id="mw-whatlinkshere-list")
	list_els = results.find_all("li")
	print(f'Found {len(list_els)} in page {pages}.')
	for list_el in list_els:
		page_name = list_el.find("a").text
		f.write(page_name + '\n')
	next_link = results.parent.find('a', string=lambda text: "next 500" in text.lower())
	if next_link:
		url = 'https://fridaynightfunking.fandom.com' + next_link.attrs['href']
	else: 
		url = ""

f.close()