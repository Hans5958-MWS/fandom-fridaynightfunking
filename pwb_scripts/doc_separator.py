#!/usr/bin/python3
"""
Separates template documentation to the `/doc` subpage.

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always     The bot won't ask for confirmation when putting a page
-summary    Set the action summary message for the edit.
-major      If used, the edit will be saved without the "minor edit" flag

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
import re
import mwparserfromhell
from mwparserfromhell.wikicode import Wikicode, Template
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': 'Separate template documentation',
	'minor': True,
}
NOINCLUDE_RE = re.compile('<noinclude>(.*?)<\/noinclude>', re.DOTALL)

class DocSeparator(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
	
):

	"""
	Separates template documentation to the `/doc` subpage.
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: 'pywikibot.page.BasePage') -> bool:
		title = page.title()
		text = page.text

		page_doc = pywikibot.Page(self.site, title + "/doc")
		if page_doc.exists():
			pywikibot.output(f"{title}/doc already exists! Skipping...")
			return True

		if len(re.findall(NOINCLUDE_RE, text)) != 1:
			pywikibot.output(f"{title} has more than 1 or none noinclude tags! Skipping...")
			return True

		text_doc = NOINCLUDE_RE.search(text).group(1)
		code: Wikicode = mwparserfromhell.parse(text)

		documentation_template = code.filter_templates(
			matches=lambda x: x.name.matches('Documentation'),
		)

		if documentation_template:
			pywikibot.output(f"{title} already has {{Delete}} tag! Skipping...")
			return True

		delete_template = code.filter_templates(
			matches=lambda x: x.name.matches('Delete'),
		)

		if delete_template:
			pywikibot.output(f"{title} already has {{Delete}} tag! Skipping...")
			return True


		if ("<infobox" in text):
			pywikibot.output(f"{title} may be an infobox. Skipping...")
			return True

		page.protection()
		if not page.has_permission():
			pywikibot.warning(f"{title} is protected: this account can't edit it! Skipping...")
			return True


		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""

		page = self.current_page

		title = page.title()
		text = page.text
		# print(title)
		# print(text)

		text_doc = NOINCLUDE_RE.search(text).group(1)
		text = NOINCLUDE_RE.sub('<noinclude>{{Documentation}}</noinclude>', text)
		# print(text)
		# print(text_doc)

		cat_texts = re.findall(r'^\s*\[\[Category:.+?\]\]\s*$', text_doc)
		text_doc = re.sub(r'^\s*\[\[Category:.+?\]\]\s*$', '', text_doc)
		print(cat_texts)
		
		if (cat_texts):
			sandbox_other = Template('Sandbox other')
			sandbox_other.add(1, "")
			sandbox_other.add(2, '\n<!-- Template categories and interwiki below this line, please. -->\n' + '\n'.join(cat_texts) + '\n')
			if text_doc:
				text_doc += '\n\n' + str(sandbox_other)
			else:
				text_doc = str(sandbox_other)

		page_doc = pywikibot.Page(page.site, title + "/doc")
		
		if self.put_current(text, summary=self.opt.summary, minor=self.opt.minor):
			self.userPut(page_doc, '', text_doc, summary=self.opt.summary, minor=self.opt.minor)
			# pass

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = DocSeparator(generator=gen, **args)
		bot.run()

if __name__ == '__main__':
	main()
