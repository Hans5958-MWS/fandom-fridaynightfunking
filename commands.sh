echo "This is for reading only! Do not run directly!"
exit 0

# Fresh start? Get Pywikibot on the pwb folder and run these below.
pwb generate_family_file
pwb generate_user_files

# Login to user account
pwb login

# Commands (remove -simulate if you want the real deal)
pwb doc_separator -start:Template:! -ns:Template -titleregexnot:\/doc$ -simulate
pwb tldr_updater -page:"Indie Cross" -simulate
pwb tldr_updater -page:"Vs_/v/-tan" -simulate
pwb tldr_updater -recentchanges:20 -ns:0 -simulate
pwb songinfo_updater -page:"Indie Cross" -simulate
pwb songinfo_updater -recentchanges:20 -ns:0  -simulate
pwb minors/notices_deprecated_parameters -start:! -simulate