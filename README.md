# MWS - Funkipedia Mods

This is the collection of scripts I use for editing in [fridaynightfunking.fandom.com](https://fridaynightfunking.fandom.com).

## Contents

| Name             | Type                   | Description                                                             | General usage? |
| ---------------- | ---------------------- | ----------------------------------------------------------------------- | -------------- |
| download-links   | AutoWikiBrowser config | Replaces wikitext download links with the premade templates.            |
| doc_separator    | Pywikibot script       | Separates template documentation to the `/doc` subpage.                 | ✅              |
| tldr_updater     | Pywikibot script       | Updates {{TLDR}} values based on existing notices.                      |
| songinfo_updater | Pywikibot script       | Migrates all song info templates with the newer, singular {{SongInfo}}. |
| minors           | Pywikibot script       | Collection of minor scripts.                                            |

## License

MIT